// get all
	fetch("https://jsonplaceholder.typicode.com/todos",{method:"GET"})
	.then(response => response.json())
	.then(result => {
		let title = result.map(element => element.title)
		console.log(title);
	})

// get single
	fetch("https://jsonplaceholder.typicode.com/todos/1",{method:"GET"})
	.then(response => response.json())
	.then(result => {console.log(`Title: ${result.title}, Status: ${result.completed}`);
	})

// post

	fetch("https://jsonplaceholder.typicode.com/todos",
		{method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				    userId: 1,
				    id: 201,
				    title: "Added",
				    completed: false
						})
		})
	.then(response => response.json())
	.then(result => console.log(result));

// put

		fetch("https://jsonplaceholder.typicode.com/todos/1",
		{
			method: "PUT",
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				title: "PUT update",
				description: "Updated using PUT method",
				status: true,
				datecompleted: "1/1/21",
				userId: 1
			})
		})
	.then(response => response.json())
	.then(result => console.log(result));


// patch
/*
change status
add date of change status
*/

	fetch("https://jsonplaceholder.typicode.com/todos/1",
		{
			method: "PATCH",
			headers: {
				'Content-Type': "application/json"
			},
			body: JSON.stringify({
				completed: true,
				datecompleted: "2/2/22"
			})
		})
	.then(response => response.json())
	.then(result => console.log(result));

// delete
	fetch("https://jsonplaceholder.typicode.com/todos/201",{method: "DELETE"})
	.then(response => response.json())
	.then(result => console.log(result));